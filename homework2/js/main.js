document.querySelectorAll(".mobile-menu").forEach(btn =>
    btn.addEventListener("click", function (event) {
        event.preventDefault();
        btn.classList.toggle("active");
    })
);

var menuElem = document.getElementById('menuBtn');
var titleElem = menuElem.querySelector('.mobile-menu');

titleElem.onclick = function () {
    menuElem.classList.toggle('open');
};
